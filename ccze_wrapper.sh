#!/bin/bash 

# ccze wrapper
function ccze_wrapper() {
    if hash ccze 2>/dev/null; then
        ccze -A
    else
	tee
    fi
}

export CCZE="ccze_wrapper"

#!/bin/bash

# $1: BBPATH as it should be
# $2: PATH where the example was e.g. ${HERE}

#set -x
function is_bb_eq () {
if [ -z "$BBPATH" ]
then
      echo "\$BBPATH is empty"
      plus_echo_red "Error: cooker env is not set."
      plus_echo_green "source /workdir/resy-cooker.sh"
      exit 1
else
      if [[ $BBPATH = ${1} ]]; then
        echo "\$BBPATH contains ${1}"
        plus_echo "which is probably good here"
      else
        plus_echo_red "Error: \$BBPATH contains ${BBPATH} but should contain ${1}"
        plus_echo_green "exit the container and restart it:"
        plus_echo_green "cd ~/projects/resy-playground/ && ./resy-poky-container.sh"
        plus_echo_green "in the new container:"
        plus_echo_green "source /workdir/resy-cooker.sh"
        plus_echo_green "cd ${2}"
        exit 1
      fi
fi
}
#set +x



#!/bin/bash

#set -x
function is_bb_not_avail () {
if [ -z "$BBPATH" ]
then
      echo "\$BBPATH is empty"
      plus_echo_green "cooker env is not set, which is good here"
else
      echo "\$BBPATH is NOT empty"
      plus_echo_red "bitbake it probably available, which is not good here"
      plus_echo_green "exit the container and restart it:"
      plus_echo_green "exit (maybe more than once needed)"
      plus_echo_green "cd ~/projects/resy-playground/ && ./resy-poky-container.sh"
      plus_echo_green "in the new container:"
      plus_echo_green "source /workdir/resy-cooker.sh"
      plus_echo_green "cd ${1}"
      exit 1
fi
}
#set +x

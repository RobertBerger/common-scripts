#!/bin/bash

#set -x
function is_bb_avail {
if [ -z "$BBPATH" ]
then
      echo "\$BBPATH is empty"
      plus_echo_red "Error: cooker env is not set."
      plus_echo_green "source /workdir/resy-cooker.sh"
      exit 1
else
      echo "\$BBPATH is NOT empty"
      plus_echo "bitbake it probably available"
fi
}
#set +x
